@extends('layout')
@section('main')
    <h1 class="text-center">Recherche avancée</h1>
    <form class="grid-container" action="{{ route('search') }}" method="get">
        <div class="grid-x grid-padding-x">
            <label class="medium-9 cell">Mots-clefs
                <input name="search" type="search" placeholder="Rechercher..." value="{{ request()->get('search') }}"/>
            </label>
            <label class="medium-3 cell">Mode de recherche
                <select name="mode">
                    <option @if(request()->get('mode') === 'all') selected @endif value="all">Tout les mots</option>
                    <option @if(request()->get('mode') === 'any') selected @endif value="any">Au moins un mot</option>
                </select>
            </label>
        </div>
    </form>
    @isset($results)
    <p class="text-center">
        <em>
            Il y a {{ $nb }} {{ $nb > 1 ? 'résultats' : 'résultat' }} pour votre recherche.
        </em>
    </p>
    <section class="grid-container">
        <div class="grid-x grid-margin-x medium-up-3 small-up-3">
            @foreach($results->getElementsByTagName('resultat') as $result)
                @include('fragments.resultat')
            @endforeach
        </div>
    </section>
    @endisset
@endsection
