<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Les minutes du procès de Nuremberg</title>
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="/css/foundation.min.css" />
</head>
<body>
@include('fragments.nav')
<main>
    @yield('main')
</main>
<footer class="text-center">
    <a target="_blank" href="http://www.unicaen.fr/recherche/mrsh/crdfed"> <img class="logo" src="/skin/sponsors/CRDFED.jpg" alt="CRDFED"></a>
    <a target="_blank" href="http://www.unicaen.fr/recherche/mrsh/"><img class="logo" src="/skin/sponsors/mrsh.jpg" alt="MRSH"></a>
    <a target="_blank" href="http://www.unicaen.fr/"><img class="logo" src="/skin/sponsors/unicaen.png" alt="Unicaen"></a>
    <a target="_blank" href="http://www.cnrs.fr/"><img class="logo" src="/skin/sponsors/CNRS.jpg" alt="CNRS"></a>
    <a><img class="logo" src="/skin/sponsors/europe.jpg" alt="L'Europe Foundation"></a>
</footer>
<script src="/js/jquery.min.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
