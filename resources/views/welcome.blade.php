@extends('layout')
@section('main')
    <h1 class="text-center">Les minutes du procès de Nuremberg</h1>
    <p>
        Le 8 août 1945, les États-Unis, le Royaume-Uni, l’URSS et le Gouvernement provisoire de la République française
        ont signé un traité créant et fixant le statut du Tribunal Militaire International « compétent pour juger et
        punir toutes personnes qui, agissant pour le compte des pays européens de l’Axe, auront commis, individuellement
        ou à titre de membres d’organisations, l’un quelconque des crimes suivants » : crimes contre la paix, crimes de
        guerre et crimes contre l’humanité. C’est sur le fondement de ce traité que s’est tenu à Nuremberg du 14
        novembre 1945 au 1er octobre 1946 le procès intenté par les puissances alliées contre 24 des principaux
        responsables politiques et militaires du Troisième Reich et contre 7 organisations nazies (dont l’unité d’élite
        SS et la Gestapo).
    </p>
    <p>
        Les minutes de ce procès ont été publiées entre 1947 et 1949 par le Secrétariat du Tribunal Militaire
        International sous la juridiction des Autorités alliées de contrôle pour l’Allemagne. Le texte officiel a été
        publié à la fois en français, en anglais et en russe. Outre de nombreux documents et annexes, le texte établit
        en 21 tomes le compte-rendu précis de chacune des 218 journées d’audience du procès, ainsi que des audiences
        préliminaires qui se sont tenues les 14, 15 et 17 novembre 1945. Cette masse documentaire importante formant un
        corpus d’environ 6 millions de mots est depuis longtemps accessible grâce à la version papier qui fut très
        largement diffusée au lendemain du procès ; elle est cependant difficile à exploiter manuellement pour les
        universitaires souhaitant effectuer des recherches sur le texte.
    </p>
    <p>
        Disposant d’une version numérisée, le Centre de recherche sur les droits fondamentaux et les évolutions du droit
        de l’université de Caen Normandie (CRDFED) a donc souhaité mettre ce texte essentiel d’abord à disposition des
        chercheurs, puis du grand public. Le procès des dignitaires nazis marque en effet un moment fondateur pour le
        développement d’une justice pénale internationale. Si aujourd’hui les Tribunaux pénaux internationaux ad hoc et
        la Cour pénale internationale se fondent sur d’autres textes que le statut de 1945, les minutes de ce procès
        restent un outil incontournable pour tout juriste travaillant sur cette question. La lecture des minutes permet
        en outre au grand public d’appréhender à la fois le système mis en place par les Nazis pendant l’occupation et
        la manière dont fut conduit le procès.
    </p>
    <p>
        Grâce à l’association <i>L’Encyclopédie universelle des droits de l’homme</i> qui a procédé à la numérisation
        des
        minutes du procès de Nuremberg d’après les volumes prêtés à cet effet par le Ministère des Affaires étrangères,
        le CRDFED, avec le concours de la Maison de la Recherche en Sciences Humaines de l’université de Caen Normandie,
        met ainsi progressivement en ligne les tomes I à XXI des minutes de ce procès en langue française. La version en
        mode texte des minutes du procès est présentée à côté de la version officielle en mode image au format PDF.
        Marie-Sophie Peyre, agissant pour le compte de l’association <i>L’Encyclopédie universelle des droits de l’homme
        </i>, a cédé l’usage non exclusif de la version numérisée des minutes à l’université de Caen Normandie. Nous la
        remercions très sincèrement pour ce précieux apport.
    </p>
@endsection

