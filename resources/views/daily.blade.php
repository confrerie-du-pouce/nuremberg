@extends('layout')
@section('main')
    <h1 class="text-center">Journée du procès de Nuremberg</h1>
    <form id="form" method="get" action="{{ route('daily') }}">
        <label>Journée
            <select onchange="submit()" name="document">
                <option></option>
                @foreach($days as $day)
                    <option @if(request()->get('document') === $day[0]) selected @endif value="{{ $day[0] }}">{{ $day[1] }}</option>
                @endforeach
            </select>
        </label>
    </form>
    @isset($document)
    @include('fragments.document')
    @endisset
@endsection
