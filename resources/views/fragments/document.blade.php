<article>
    <h2>{{ $document["title"] }}<small>{{ $document["date"] }}</small></h2>
    @foreach($document['speakers'] as $speaker)
        <article>
            <h5>{{ $speaker->getElementsByTagName('speaker')[0]->textContent }}</h5>
            <blockquote>
                @foreach($speaker->getElementsByTagName('p') as $p)
                    <p>{{ $p->textContent }}</p>
                @endforeach
            </blockquote>
        </article>
    @endforeach
</article>
