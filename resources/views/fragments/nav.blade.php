<nav class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menu-text">Procès de Nuremberg</li>
            <li>
                <a href="{{ route('home') }}">Accueil</a>
            </li>
            <li>
                <a href="{{ route('daily') }}">Journée</a>
            </li>
            <li>
                <a href="{{ route('search') }}">Rechercher</a>
            </li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
            <li>
                <form id="global" method="get" action="{{ route('search') }}">
                    <label>
                        <input name="search" type="search" placeholder="Rechercher..." />
                    </label>
                </form>
            </li>
            <li>
                <button onclick="document.getElementById('global').submit()" type="button" class="button">
                    Rechercher
                </button>
            </li>
        </ul>
    </div>
</nav>

