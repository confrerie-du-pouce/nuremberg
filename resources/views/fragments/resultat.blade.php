<article class="cell card">
    <h5 class="text-center">
        <a href="{{ route('daily') }}?document={{ $result->getElementsByTagName('document')[0]->textContent }}">
            {{ $result->getElementsByTagName('title')[0]->textContent }}
        </a>
    </h5>
    <p>
        {{ $result->getElementsByTagName('paragraph')[0]->textContent }}
    </p>
</article>
