<?php

namespace App\Http\Controllers;

use DateTime;
use DOMDocument;
use DOMNodeList;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Caxy\BaseX\Session;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return View
     */
    public function advanced(Request $request): View
    {
        $results = new DOMDocument();
        $database = env('BASEX_DATABASE');
        if ($request->get('search') !== null) {
            $keywords = $this->normalize($request->get('search'));
            $formatted = '{"'.join('","', $keywords).'"}';
            $mode = $request->get('mode', 'all');
            $xml = '<resultats>'.
                $this->querying("for \$text in //*:p
                where \$text[text() contains text $formatted $mode
                using stemming using language \"French\"]
                return <resultat>
                    <title>{db:open('$database', db:path(\$text))//*:title/text()}</title>
                    <document>{db:path(\$text)}</document>
                    <paragraph>{\$text/text()}</paragraph>
                </resultat>").
            '</resultats>';
            $results->loadXML($xml);
        }
        return view('search', [
            'results' => $results,
            'nb' => $results->getElementsByTagName('resultat')->length
        ]);
    }

    /**
     * @param string $keywords
     * @return array
     */
    public function normalize(string $keywords): array
    {
        $keywords = strtolower($keywords);
        $keywords = preg_replace('/[^A-Za-zÀ-ÖØ-öø-ÿ]/', ' ', $keywords);
        $keywords = preg_split('/ /', $keywords);
        return $keywords;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function daily(Request $request): View
    {
        $document = $request->input('document');
        if ($document !== null)
            $document = $this->getDocument($document);
        return view('daily', [
            'days' => $this->fetchDays(),
            'document' => $document
        ]);
    }

    /**
     * @return array
     */
    private function fetchDays(): array
    {
        $result = $this->querying("for \$date in //*:date
            where (\$date/text() != \"\")
            order by \$date
            return [db:path(\$date),\$date/text()]");
        $result = explode("\n", $result);
        foreach ($result as $key => $item) {
            if ($item !== "") {
                $tokens = explode(", ", $item);
                $result[$key] = [
                    substr($tokens[0], 2, strlen($tokens[0]) - 3),
                    date_format(
                        date_create(
                            substr($tokens[1], 0, strlen($tokens[1]) - 1
                            )
                        ), "d M Y H:i")
                ];
            } else unset($result[$key]);
        }
        return $result;
    }

    private function querying(string $query): string
    {
        $result = null;
        $database = env('BASEX_DATABASE');
        try {
            $session = new Session(env('BASEX_HOST'),
                env('BASEX_PORT'),
                env('BASEX_USERNAME'),
                env('BASEX_PASSWORD'));
            $session->execute("OPEN $database");
            $query = $session->query($query);
            $result = $query->execute()."\n";
            $query->close();
            $session->close();
        } catch (Exception $e) {
            print $e->getMessage();
        }
        return $result;
    }

    private function getDocument(string $doc): array
    {
        $xml = new DOMDocument();
        $xml->load(asset("Nuremberg/$doc"), );
        $document = array();
        $document['title'] = $this->returnFirstFilled($xml->getElementsByTagName('title'));
        $date = $this->returnFirstFilled($xml->getElementsByTagName('date'));
        try {
            $document['date'] = date_format(new DateTime($date), 'd M Y H:i');
        } catch (Exception $e) {
        }
        $document['speakers'] = $xml->getElementsByTagName('sp');
        return $document;
    }

    private function returnFirstFilled(DOMNodeList $items): string
    {
        foreach ($items as $item) {
            if ($item->textContent === "") continue;
            return $item->textContent;
        }
    }
}
